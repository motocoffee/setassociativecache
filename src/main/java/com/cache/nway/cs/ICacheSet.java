package com.cache.nway.cs;


/**
 * 
 * @author Vijayesh
 *
 * @param <K>
 * @param <V>
 */
public interface ICacheSet<K, V> {
	/**
	 * 
	 * @param k
	 * @return
	 */
	public V get(K k);
	/**
	 * 
	 * @param k
	 * @param v
	 */
	public void put(K k, V v);

}

