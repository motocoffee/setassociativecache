package com.cache.nway.main;

import com.cache.nway.nscache.INWSAssociativeCache;
import com.cache.nway.nscache.NWSAssociativeCache;
import com.cache.nway.nwset.NWSet;
import com.cache.nway.ra.LRURA;

public class NWSACacheApplication {

	public static void main(String args[]) throws Exception {
		INWSAssociativeCache<Integer, Integer> testCache = null;
		try {
			//uses LRU Replacement Algorithm and user defined set mapper. In this case just defaulted all entries to set 0 for testing purposes
			testCache = new NWSAssociativeCache<>(10, 4, LRURA.class, new NWSet<Integer>() {
				@Override
				public int getSetNo(Integer k, Integer setSize) {
					return 0;
				}

			});
		} catch (InstantiationException | IllegalAccessException e) {
			e.printStackTrace();
		}

		testCache.put(1, 1);
		testCache.put(2, 2);
		testCache.put(3, 3);
		testCache.put(4, 4);

		testCache.get(1);
		testCache.get(2);

		testCache.put(5, 5);
		testCache.put(6, 6);
		
		testCache.get(1);
		testCache.get(2);
		
		testCache.put(7, 7);
		testCache.put(8, 8);
		
		int i = 1;

		System.out.println("As per the access the cache should contain 1, 2, 7 and 8");
		System.out.println("Cache contents are below:");
		Integer val = null;
		while ( i <= 8 ) {
			val = testCache.get(i);
			if (val != null) {
				System.out.println(val);
			}
			i++;
		}
	}
}

