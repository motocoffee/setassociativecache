package com.cache.nway.nscache;

/**
 * 
 * @author Vijayesh
 *
 * @param <K>
 * @param <V>
 */
public interface INWSAssociativeCache<K, V> {
	/**
	 * 
	 * @param k
	 * @param v
	 */
	public void put(K k, V v);
	/**
	 * 
	 * @param k
	 * @return
	 */
	public V get(K k);

}

