package com.cache.nway.nscache;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.FutureTask;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantReadWriteLock;

import com.cache.nway.cs.ICacheSet;
import com.cache.nway.nwset.NWSet;
import com.cache.nway.ra.LRURA;
import com.cache.nway.ra.ReplacementAlgorithm;

/**
 * 
 * @author Vijayesh
 *
 * @param <K>
 * @param <V>
 */
public class NWSAssociativeCache<K, V> implements INWSAssociativeCache<K, V> {

	private List<CacheSet<K, V>> mem;
	private int memSize;
	private NWSet<K> nwSet;
	private final Thread backUpService = new Thread(new CacheBacKUpService<K>());
	
	/**
	 * 
	 * @author Vijayesh
	 *
	 * @param <K>
	 */
	static class CacheBacKUpService<K> implements Runnable {
		@Override
		public void run() {
			try {
				Thread.sleep(100000);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			System.out.println("Taking backup");
		}
	}
	
	/**
	 * 
	 * @param memSize
	 * @param size
	 * @throws InstantiationException
	 * @throws IllegalAccessException
	 */
	public NWSAssociativeCache(int memSize, int size) throws InstantiationException, IllegalAccessException {
		this(memSize,size, LRURA.class, new NWSACacheSet<K>());
	}
	/**
	 * 
	 * @param memSize
	 * @param size
	 * @param nwSet
	 * @throws InstantiationException
	 * @throws IllegalAccessException
	 */
	public NWSAssociativeCache(int memSize, int size, NWSet<K> nwSet ) throws InstantiationException, IllegalAccessException {
		this(memSize,size, LRURA.class, nwSet);
	}
	/**
	 * 
	 * @param memSize
	 * @param size
	 * @param algo
	 * @throws InstantiationException
	 * @throws IllegalAccessException
	 */
	public NWSAssociativeCache(int memSize, int size, Class<? extends ReplacementAlgorithm> algo) throws InstantiationException, IllegalAccessException {
		this(memSize,size, algo, new NWSACacheSet<K>());
	}
	/**
	 * 
	 * @param memSize
	 * @param size
	 * @param algo
	 * @param nwSet
	 * @throws InstantiationException
	 * @throws IllegalAccessException
	 */
	public NWSAssociativeCache(int memSize, int size, Class<? extends ReplacementAlgorithm> algo, NWSet<K> nwSet ) throws InstantiationException, IllegalAccessException {
		this.memSize = memSize;
		mem = new ArrayList<>(memSize);
		int i = 0;
		while (i < memSize) {
			try {
				mem.add(new CacheSet<K, V>(size, i, algo.newInstance()));
				i++;
			} catch (InstantiationException e) {
				e.printStackTrace();
				throw e;
			} catch (IllegalAccessException e) {
				e.printStackTrace();
				throw e;
			}
		}
		this.nwSet = nwSet;
		backUpService.start();
	}

	@Override
	public void put(K k, V v) {
		int setNo = nwSet.getSetNo(k, memSize);
		CacheSet<K, V> cs = mem.get(setNo);
		cs.put(k, v);
	}

	@Override
	public V get(K k) {
		int setNo = nwSet.getSetNo(k, memSize);
		CacheSet<K, V> cs = mem.get(setNo);
		V v = cs.get(k);
		return v;
	}

	/**
	 * 
	 * @author Vijayesh
	 *
	 * @param <K>
	 * @param <V>
	 */
	static class CacheSet<K, V> implements ICacheSet<K, V> {

		private final ReentrantReadWriteLock rwl = new ReentrantReadWriteLock();
		private final Lock r = rwl.readLock();
		private final Lock w = rwl.writeLock();
		private final ReplacementAlgorithm<K> rAlgo;
		private final ExecutorService exService = Executors.newSingleThreadExecutor();
		/**
		 * 
		 */
		CacheSet() {
			size = 10;
			this.rAlgo = null;
		}
		/**
		 * 
		 * @param size
		 * @param setLocation
		 * @param rAlgo
		 */
		CacheSet(int size, int setLocation, ReplacementAlgorithm<K> rAlgo) {
			this.size = size;
			this.rAlgo = rAlgo;
			this.setNo = setLocation;
		}

		Map<K, V> lines = new HashMap<>();

		int size;

		int count;

		int setNo;

		@Override
		public V get(K k) {
			V v = null;
			try {
				r.lock();
				v = lines.get(k);
				if (v != null) {
					callAsync(false, k);
				}
			} catch (Exception e) {
				e.printStackTrace();
			} finally {
				r.unlock();
			}
			return v;	 
		}

		@Override
		public void put(K k, V v) {
			try {
				w.lock();
				if (this.lines.containsKey(k)) {
					this.lines.put(k, v);
					callAsync(false, k);
				} else {
					if (this.count >= this.size) {
						FutureTask<K> futureTask = new FutureTask<K>(new CacheCallable<K>(rAlgo));
						exService.execute(futureTask);
						while(!futureTask.isDone());
						K k1 = futureTask.get();
						lines.remove(k1);
						count--;
					}
					this.lines.put(k, v);
					count++;
					callAsync(true, k);
				}
			} catch (Exception e) {
				e.printStackTrace();
			} finally {
				w.unlock();
			}
		}

		/**
		 * 
		 * @param isWrite
		 * @param k
		 */
		private void callAsync(final boolean isWrite, final K k) {
			exService.execute(new Runnable() {
				public void run() {
					if (isWrite) {
						rAlgo.write(k);
					} else {
						rAlgo.read(k);
					}
				}
			});
		}
	}

	/**
	 * 
	 * @author Vijayesh
	 *
	 */
	static class NWSACacheSet<K> implements NWSet<K> {

		@Override
		public int getSetNo(K k, Integer setSize) {
			return hash(k) % setSize;
		}

		/**
		 * 
		 * @param k
		 * @return
		 */
		private int hash(K k) {
			int hash = k.hashCode() ^ (k.hashCode() >>> 16) ^ (k.hashCode() >>> 8) ^ (k.hashCode() >>> 4)
					^ (k.hashCode() >>> 2);
			return hash;
		}
	}

	/**
	 * 
	 * @author Vijayesh
	 *
	 * @param <K>
	 */
	static class CacheCallable<K> implements Callable<K> {
		private ReplacementAlgorithm<K> algo;
		public CacheCallable(ReplacementAlgorithm<K> algo){
			this.algo = algo;
		}
		@Override
		public K call() throws Exception {
	        return algo.remove();
		}

	}

}

