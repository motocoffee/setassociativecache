package com.cache.nway.nwset;

/**
 * 
 * @author Vijayesh
 *
 * @param <K>
 */
public interface NWSet<K> {
	/**
	 * 
	 * @param k
	 * @param setSize
	 * @return
	 */
	public int getSetNo(K k, Integer setSize);
}