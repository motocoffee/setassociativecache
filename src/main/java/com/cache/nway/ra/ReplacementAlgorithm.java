package com.cache.nway.ra;

/**
 * 
 * @author Vijayesh
 *
 * @param <K>
 */
public interface ReplacementAlgorithm<K> {
	/**
	 *
	 * @param k
	 */
	public void read(K k);
	/**
	 * 
	 * @param k
	 */
	public void write(K k);
	/**
	 * 
	 * @return
	 */
	public K remove();
	
}

