package com.cache.nway.ra;

import java.util.HashMap;
import java.util.Map;

public class MRURA<K> implements ReplacementAlgorithm<K> {
	/**
	 * 
	 * @author Vijayesh
	 *
	 * @param <K>
	 */
	static class Node<K> {
		private Node<K> left;
		private Node<K> right;
		private K k;

		Node(Node<K> left, Node<K> right, K k) {
			this.left = left;
			this.right = right;
			this.k = k;
		}

		public boolean equals(Object o) {
			if (o instanceof LRURA.Node) {
				MRURA.Node n = (MRURA.Node) o;
				return this.k.equals(n.k);
			}
			return false;
		}
	}

	Node<K> startNode = null;
	Node<K> endNode = null;
	Map<K, Node<K>> map = new HashMap<>();
	
	@Override
	public void read(K k) {
		synchronized (this) {
			if (map.containsKey(k)) {
				Node<K> node = map.get(k);
				if (node.equals(endNode)) {
					return;
				}
				if (node.equals(startNode)) {
					startNode = startNode.right;
				}
				node.right.left = node.left;
				node.right = null;
				endNode.right = node;
				node.left = endNode;
				endNode = node;
			}
		}
		
	}

	@Override
	public void write(K k) {
		synchronized (this) {
			if (endNode == null) {
				endNode = new Node<>(null, null, k);
				startNode = endNode;
			} else {
				Node<K> n = new Node<>(endNode, null, k);
				endNode.right = n;
				endNode = n;
			}
			map.put(k, endNode);
		}
		
	}

	@Override
	public K remove() {
		K k = null;
		synchronized (this) {
			if (startNode != null) {
				if (startNode.equals(endNode)) {
					k = startNode.k;
					startNode = null;
					endNode = null;
				} else {
					startNode.right.left = null;
					k = startNode.k;
					startNode = startNode.right;
				}
				map.remove(k);
			}
			return k;
		}
	}

}

