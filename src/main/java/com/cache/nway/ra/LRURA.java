package com.cache.nway.ra;

import java.util.HashMap;
import java.util.Map;
/**
 * 
 * @author Vijayesh
 *
 * @param <K>
 */
public class LRURA<K> implements ReplacementAlgorithm<K> {

	/**
	 * 
	 * @author Vijayesh
	 *
	 * @param <K>
	 */
	static class Node<K> {
		private Node<K> left;
		private Node<K> right;
		private K k;

		Node(Node<K> left, Node<K> right, K k) {
			this.left = left;
			this.right = right;
			this.k = k;
		}

		public boolean equals(Object o) {
			if (o instanceof LRURA.Node) {
				LRURA.Node n = (LRURA.Node) o;
				return this.k.equals(n.k);
			}
			return false;
		}
	}

	Node<K> startNode = null;
	Node<K> endNode = null;
	Map<K, Node<K>> map = new HashMap<>();

	@Override
	public void read(K k) {
		synchronized (this) {
			if (map.containsKey(k)) {
				Node<K> node = map.get(k);
				if (node.equals(startNode)) {
					return;
				}
				if (node.equals(endNode)) {
					endNode = endNode.left;
					//endNode.right = null;
				}
				node.left.right = node.right;
				node.left = null;
				startNode.left = node;
				node.right = startNode;
				startNode = node;
			}
		}
	}

	@Override
	public void write(K k) {
		synchronized (this) {
			if (startNode == null) {
				startNode = new Node<>(null, null, k);
				endNode = startNode;
			} else {
				Node<K> n = new Node<>(null, startNode, k);
				startNode.left = n;
				startNode = n;
			}
			map.put(k, startNode);
		}

	}

	@Override
	public K remove() {
		K k = null;
		synchronized (this) {
			if (endNode != null) {
				if (endNode.equals(startNode)) {
					k = endNode.k;
					startNode = null;
					endNode = null;
				} else {
					endNode.left.right = null;
					k = endNode.k;
					endNode = endNode.left;
				}
				map.remove(k);
			}
			return k;
		}
	}

}

