package com.cache.nway.ra;



import java.util.ArrayDeque;
import java.util.Deque;
/**
 * 
 * @author Vijayesh
 *
 * @param <K>
 */
public class FIFORA<K> implements ReplacementAlgorithm<K> {

	private Deque<K> queue = new ArrayDeque<>();

	@Override
	public void read(K k) {
	}

	@Override
	public void write(K k) {
		synchronized (this) {
			queue.add(k);
		}
	}

	@Override
	public K remove() {
		K k = null;
		synchronized (this) {
			k = queue.pop();
			return k;
		}
	}

}
